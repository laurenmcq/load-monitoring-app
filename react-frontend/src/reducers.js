import { combineReducers } from 'redux';
import { freeze } from 'icepick';

import loadMonitoringReducer from './load-monitor/load-monitor-reducer';

const initialState = freeze({
    entities: undefined,
});

export default (state = initialState, action) =>
    freeze(
        combineReducers({
            entities: loadMonitoringReducer,
        })(state, action),
    );
