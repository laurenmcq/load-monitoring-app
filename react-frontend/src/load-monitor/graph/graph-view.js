import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';
import { LOAD_THRESHOLD } from '../constants';

const Container = styled.div`
    padding-right: 40px;
`;

export default class GraphView extends Component {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.object),
    };

    static defaultProps = {
        data: [],
    };
     
    render() {
        return (
            <Container>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart 
                data={this.props.data}
            >
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip/>
                <Legend />
                <Line type="monotone" dataKey="value" stroke="#8884d8"/>
                <ReferenceLine y={LOAD_THRESHOLD} stroke="red" strokeDasharray="3 3" />
            </LineChart>
            </ResponsiveContainer>
            </Container>
        );
    }
}
