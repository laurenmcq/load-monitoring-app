
export const FETCH_EVENTS_REQUEST = 'FETCH_EVENTS_REQUEST';
export const FETCH_EVENTS_SUCCESS = 'FETCH_EVENTS_SUCCESS';
export const FETCH_EVENTS_CANCEL = 'FETCH_EVENTS_CANCEL';

export const FETCH_ALERTS_REQUEST = 'FETCH_ALERTS_REQUEST';
export const FETCH_ALERTS_SUCCESS = 'FETCH_ALERTS_SUCCESS';
export const FETCH_ALERTS_CANCEL = 'FETCH_ALERTS_CANCEL';

export const fetchEventsRequest = () => ({
    type: FETCH_EVENTS_REQUEST,
});

export const fetchEventsSuccess = payload => ({
    type: FETCH_EVENTS_SUCCESS,
    payload,
});

export const fetchEventsCancel = () => ({
    type: FETCH_EVENTS_CANCEL,
});

export const fetchAlertsRequest = () => ({
    type: FETCH_ALERTS_REQUEST,
});

export const fetchAlertsSuccess = payload => ({
    type: FETCH_ALERTS_SUCCESS,
    payload,
});

export const fetchAlertsCancel = () => ({
    type: FETCH_ALERTS_CANCEL,
});
