import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import BootstrapTable from 'react-bootstrap-table-next';

const Container = styled.div`
    padding: 20px 20px 40px 40px;
`;

const columns = [
    {
        dataField: 'datetime',
        text: 'Date',
        sort: true
    }, 
    {
        dataField: 'loadAverage',
        text: 'Load Average',
        sort: true
    }, 
    {
        dataField: 'systemUptime',
        text: 'System Uptime (s)',
        sort: true
    }, 
    {
        dataField: 'percentFreeMemory',
        text: 'Free Memory (%)',
        sort: true
    }
];

export default class EventView extends Component {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.object),
    };

    static defaultProps = {
        data: [],
    };

    render() {
        return (
            <Container>
                <h4>Events</h4>
                <BootstrapTable 
                    keyField="datetime"
                    data={this.props.data} 
                    columns={columns} 
                />
            </Container>
        );
    }
}






