import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { 
    fetchEventsRequest, 
    fetchAlertsRequest,
    fetchEventsCancel,
    fetchAlertsCancel,
} from './load-monitor-actions';
import Graph from './graph/graph-view';
import Events from './events/events-view';
import Alerts from './alerts/alerts-view';
import Status from './status/status-view';
import { 
    eventsDataSelector, 
    alertsSelector, 
    graphDataSelector ,
    lastEventSelector,
} from './load-monitor-selector';

const Heading = styled.h2`
    text-align: center;
    Padding: 20px;
`;

const Flexbox = styled.div`
    display: flex;
    justify-content: space-between;
`;

const EventsFlex = styled.div`
    flex-grow: 2;
`;

const AlertsFlex = styled.div`
    flex-grow: 1;
`;

export class LoadMonitor extends Component {
    static propTypes = {
        onWillMount: PropTypes.func.isRequired,
        onWillUnMount: PropTypes.func.isRequired,
        graphData: PropTypes.arrayOf(PropTypes.object),
        eventData: PropTypes.arrayOf(PropTypes.object),
        alertData: PropTypes.arrayOf(PropTypes.object),
        lastEvent: PropTypes.object,
    };

    componentWillMount() {
        this.props.onWillMount();
    }

    componentWillUnmount() {
        this.props.onWillUnMount();
    }

    render() {
        return (
            <div>
                <Heading>Load Average Monitoring</Heading>
                <Status lastEvent={this.props.lastEvent}/>
                <Graph data={this.props.graphData}/>
                <Flexbox>
                    <EventsFlex>
                        <Events data={this.props.eventData}/>
                    </EventsFlex>
                    <AlertsFlex>
                        <Alerts data={this.props.alertData}/>
                    </AlertsFlex>
                </Flexbox>
            </div>
        );
    }
}

export default connect(
    state => ({
        graphData: graphDataSelector(state),
        eventData: eventsDataSelector(state),
        alertData: alertsSelector(state), 
        lastEvent: lastEventSelector(state),
    }),
    dispatch => ({
        onWillMount: () => {
            dispatch(fetchEventsRequest());
            dispatch(fetchAlertsRequest());
        },
        onWillUnMount: () => {
            dispatch(fetchEventsCancel());
            dispatch(fetchAlertsCancel());
        },
    }),
)(LoadMonitor);
