import { Observable } from 'rxjs';
import { combineEpics } from 'redux-observable';
import { 
    FETCH_EVENTS_REQUEST, 
    FETCH_ALERTS_REQUEST,
    FETCH_EVENTS_CANCEL,
    FETCH_ALERTS_CANCEL,
    fetchEventsSuccess,
    fetchAlertsSuccess, 
} from './load-monitor-actions';

const eventsEpic = (action$, store) =>
    action$.ofType(FETCH_EVENTS_REQUEST)
    .mergeMap(action =>
        Observable
            .interval(10000)
            .startWith(0)
            .switchMap(() => {
                return fetch('/api/events')
                    .then(response => response.json())
                    .then(response => fetchEventsSuccess(response))
                    .catch(error => {
                        //Would really log here for production
                        console.log(error);
                    });
            })
            .takeUntil(action$.ofType(FETCH_EVENTS_CANCEL))
    );

const alertsEpic = (action$, store) =>
    action$.ofType(FETCH_ALERTS_REQUEST)
    .mergeMap(action =>
        Observable
            .interval(10000)
            .startWith(0)
            .switchMap(() => {
                return fetch('/api/alerts')
                    .then(response => response.json())
                    .then(response => fetchAlertsSuccess(response))
                    .catch(error => {
                        //Would really log here for production
                        console.log(error);
                    });
            })
            .takeUntil(action$.ofType(FETCH_ALERTS_CANCEL))
    );

export default combineEpics(
    eventsEpic,
    alertsEpic,
);
