import { createSelector } from 'reselect';

const entitiesSelector = state => state.entities;

const eventsSelector = createSelector(
    entitiesSelector,
    entities => entities.events,
);

export const alertsSelector = createSelector(
    entitiesSelector,
    entities => entities.alerts,
);

export const graphDataSelector = createSelector(
    eventsSelector,
    events => events.map(event => {
        return { 
            name: (new Date(event.datetime)).toLocaleTimeString(), 
            value: Number(event.loadAverage) 
        };
    }).reverse(),
);

export const eventsDataSelector = createSelector(
    eventsSelector,
    events => events.map(event => {
        return { 
            ...event,
            datetime: (new Date(event.datetime)).toLocaleString()
        };
    }),
);

export const lastEventSelector = createSelector(
    eventsSelector,
    events => events[0],
);
