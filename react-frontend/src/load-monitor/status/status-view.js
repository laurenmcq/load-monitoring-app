import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Lozenge from '@atlaskit/lozenge';
import { LOAD_THRESHOLD } from '../constants';

const Container = styled.div`
    padding: 0 0 10px 40px;
`;

export default class StatusView extends Component {
    static propTypes = {
        lastEvent: PropTypes.object,
    };

    static defaultProps = {
        lastEvent: null,
    };

    render() {
        if(!this.props.lastEvent) {
            return null;
        }

        let status;
        if(this.props.lastEvent.loadAverage > LOAD_THRESHOLD) {
            status = <h4>Status: <Lozenge appearance="removed">Warning</Lozenge> - Load is high on host</h4>;    
        } else {
            status = <h4>Status: <Lozenge appearance="success">OK</Lozenge> - Load is normal on host</h4>;
        }

        return (
            <Container>
                {status}
            </Container>
        );
    }
}






