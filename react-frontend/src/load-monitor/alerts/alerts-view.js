import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Panel } from 'react-bootstrap';
import EditorSuccessIcon from '@atlaskit/icon/glyph/editor/success';
import WarningIcon from '@atlaskit/icon/glyph/warning';

const Container = styled.div`
    padding: 20px 40px 40px 20px;
`;

const Flex = styled.div`
    display: flex;
    align-items: center;
`;

export default class AlertView extends Component {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.object),
    };

    static defaultProps = {
        data: [],
    };

    getAlert = (alert) => {
        const date = (new Date(alert.datetime)).toLocaleString();
        if(alert.warning) {
            return (
                <Panel bsStyle="danger" key={date}>
                    <Panel.Heading>
                        <Panel.Title componentClass="h3">
                            <Flex><WarningIcon />Warning</Flex>
                        </Panel.Title>
                    </Panel.Heading>
                    <Panel.Body>
                        <p>High load generated an alert at {date}</p>
                        <p>Load average (last 2 minutes): {alert.loadAverage}</p>
                    </Panel.Body>
                </Panel>
            );
        }
        return (
            <Panel bsStyle="success" key={date}>
                <Panel.Heading>
                    <Panel.Title componentClass="h3"><span><EditorSuccessIcon />Recovered</span></Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                    <p>Load recovered at {date}</p>
                    <p>Load average (last 2 minutes): {alert.loadAverage}</p>
                </Panel.Body>
            </Panel>
        );
    }

    render() {
        const components = this.props.data.map(alert => {
            return this.getAlert(alert);
        });

        return (
            <Container>
                <h4>Alerts</h4>
                {components}
            </Container>
        );
    }
}






