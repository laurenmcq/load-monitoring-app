import { freeze, set } from 'icepick';
import { FETCH_EVENTS_SUCCESS, FETCH_ALERTS_SUCCESS } from './load-monitor-actions';

const initialState = freeze({
    events: [],
    alerts: [],
});

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case FETCH_EVENTS_SUCCESS:
            return set(state, 'events', payload);

        case FETCH_ALERTS_SUCCESS:
            return set(state, 'alerts', payload);

        default:
            return state;
    }
};