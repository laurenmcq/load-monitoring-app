import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { compose } from 'redux';
import rootEpic from './epics';
import rootReducer from './reducers';

export const composeWithDevtools = (devtoolsConfig = {}) => {
    const devtoolsEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    return devtoolsEnhancer ? devtoolsEnhancer(devtoolsConfig) : compose;
};

export default () =>
    createStore(
        rootReducer, 
        undefined, 
        composeWithDevtools({ name: 'Load Monitoring' })(
            applyMiddleware(createEpicMiddleware(rootEpic)),
        ),
    );
