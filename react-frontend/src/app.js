import React, { Component } from 'react';
import { Provider } from 'react-redux'
import createStore from './store';
import LoadMonitor from './load-monitor/load-monitor-view';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.store = createStore();
  }

  render() {
    return (
      <Provider store={this.store}>
        <LoadMonitor />
      </Provider>
    );
  }
}
