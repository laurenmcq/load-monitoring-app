import { combineEpics } from 'redux-observable';
import loadMonitorEpic from './load-monitor/load-monitor-fetch-epic';

export default combineEpics(loadMonitorEpic);
