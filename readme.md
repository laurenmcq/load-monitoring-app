
1. Clone this repository `git clone git@bitbucket.org:laurenmcq/load-monitoring-app.git`
2. Open a terminal and navigate to load-monitoring-app/node-backend directory
2. Run `yarn && PORT=3001 node bin/www` (this must be port 3001)
3. Open another terminal and navigate to load-monitoring-app/react-frontend directory
4. Run `yarn && yarn start`
5. The browser should open automatically, if not open http://localhost:3000/ in your browser