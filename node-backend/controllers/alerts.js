const sqlite3 = require('sqlite3').verbose();
const db = require('../db');
const LOAD_THRESHOLD = 1;

module.exports = {
    addWarningAlert(datetime, datetimeSeconds, loadAverage) {
        db.run("INSERT INTO alerts (datetime, datetimeSeconds, loadAverage, warning) VALUES (?, ?, ?, ?)", datetime, datetimeSeconds, loadAverage, 1);  
    },
    
    addRecoverAlert(datetime, datetimeSeconds, loadAverage) {
        db.run("INSERT INTO alerts (datetime, datetimeSeconds, loadAverage, warning) VALUES (?, ?, ?, ?)", datetime, datetimeSeconds, loadAverage, 0);  
    },

    triggerAlert(datetime, datetimeSeconds, loadAverage, row) {
        if(!row || row.warning === 0) { // no previous alerts or last alert was a recovery alert
            if(loadAverage > LOAD_THRESHOLD) {
                this.addWarningAlert(datetime, datetimeSeconds, loadAverage);
            }
        } else if(row && row.warning === 1) { //last alert was a warning alert
            if(loadAverage <= LOAD_THRESHOLD) {
                this.addRecoverAlert(datetime, datetimeSeconds, loadAverage);
            }
        }
    },

    shouldTriggerAlert(datetime, datetimeSeconds) {
        db.get("SELECT avg(loadAverage) as avg FROM events WHERE datetimeSeconds > (?)", datetimeSeconds - 120, (err, row) => {
            const loadAvg = row.avg.toFixed(2);
            db.get("SELECT warning FROM alerts ORDER BY datetimeSeconds DESC LIMIT 1", (err, row) => {
                this.triggerAlert(datetime, datetimeSeconds, loadAvg, row);
            });
        });
    }
};