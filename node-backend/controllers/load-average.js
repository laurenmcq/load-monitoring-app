const os = require("os");
const sqlite3 = require('sqlite3').verbose();
const alertsController = require('./alerts');
const db = require('../db');

const insertLoadMonitoringData = (datetime, datetimeSeconds, loadAverage, sysUptime, percentMem) => {
    db.run("INSERT INTO events (datetime, datetimeSeconds, loadAverage, systemUptime, percentFreeMemory) VALUES (?, ?, ?, ?, ?)", datetime, datetimeSeconds, loadAverage, sysUptime, percentMem);
};

// Function runs every 10 seconds to get the loadAverage and other os information to save in the DB.
const saveLoadMonitoringData = () => {
    const loadAverage = os.loadavg()[0].toFixed(2);
    const datetime = new Date().toISOString();
    const datetimeSeconds = new Date().getTime() / 1000;
    const sysUptime = os.uptime();
    const percentMem = (os.freemem() / os.totalmem() * 100).toFixed(2);
  
    db.serialize(function() {
        insertLoadMonitoringData(datetime, datetimeSeconds, loadAverage, sysUptime, percentMem);
        alertsController.shouldTriggerAlert(datetime, datetimeSeconds);
    });
    setTimeout(saveLoadMonitoringData, 10000);
}

module.exports = {
    saveLoadMonitoringData
};

