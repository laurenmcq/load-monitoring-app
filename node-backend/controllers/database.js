const sqlite3 = require('sqlite3').verbose();
const db = require('../db');

const createDatabase = () => {
    db.serialize(function() {
        db.run("CREATE TABLE IF NOT EXISTS events (datetime TEXT, datetimeSeconds INTEGER, loadAverage REAL, systemUptime INTEGER, percentFreeMemory REAL)");
        db.run("CREATE TABLE IF NOT EXISTS alerts (datetime TEXT, datetimeSeconds INTEGER, loadAverage REAL, warning INTEGER)");
    });
}

module.exports = {
    createDatabase,
};