const express = require('express');
const router = express.Router();
const loadController = require('./controllers/load-average');
const database = require('./controllers/database');
const sqlite3 = require('sqlite3').verbose();
const db = require('./db');

// Set up the local DB and start adding load data to it
database.createDatabase();
loadController.saveLoadMonitoringData();

const app = express();

// Simple API
app.use('/api', router);

//get last 10 minutes
router.get('/events', function(req, res, next) {
    db.serialize(function() {
        const last10Minutes = new Date().getTime() / 1000 - (60 * 10);
        db.all("SELECT * FROM events WHERE datetimeSeconds > (?) ORDER BY datetimeSeconds DESC", last10Minutes, function(err, rows) {
            res.send(rows);
        });
    });
});

router.get('/alerts', function(req, res, next) {
    db.serialize(function() {
        db.all("SELECT * FROM alerts ORDER BY datetimeSeconds DESC LIMIT 20", function(err, rows) {
            res.send(rows);
        });
    });
});
  
module.exports = app;