const alerts = require('../controllers/alerts');

const datetime = '2018-01-32';
const datetimeSeconds = '56578787';

beforeEach(() => {
    alerts.addWarningAlert = jest.fn();
    alerts.addRecoverAlert = jest.fn();
});

afterEach(() => {
    jest.resetAllMocks();
});

test('No previous alerts, load avg > 1', () => {
    const loadAverage = 1.3;
    const row = null;

    alerts.triggerAlert(datetime, datetimeSeconds, loadAverage, row);

    expect(alerts.addWarningAlert).toHaveBeenCalledTimes(1);
    expect(alerts.addRecoverAlert).toHaveBeenCalledTimes(0);
});

test('No previous alerts, load avg <= 1', () => {
    const loadAverage = 0.8;
    const row = null;

    alerts.triggerAlert(datetime, datetimeSeconds, loadAverage, row);
    
    // Show not add an alert as no previous warning alerts
    expect(alerts.addWarningAlert).toHaveBeenCalledTimes(0);
    expect(alerts.addRecoverAlert).toHaveBeenCalledTimes(0);
});

test('Previous "Warning" alert, load avg > 1', () => {
    const loadAverage = 1.5;
    const row = { warning: 1 };

    alerts.triggerAlert(datetime, datetimeSeconds, loadAverage, row);
    
    // Show not add an alert as the last alert was a warning
    expect(alerts.addWarningAlert).toHaveBeenCalledTimes(0);
    expect(alerts.addRecoverAlert).toHaveBeenCalledTimes(0);
});

test('Previous "Warning" alert, load avg <= 1', () => {
    const loadAverage = 0.8;
    const row = { warning: 1 };

    alerts.triggerAlert(datetime, datetimeSeconds, loadAverage, row);
    
    // Show add a recovery alert as the last alert was a warning
    expect(alerts.addWarningAlert).toHaveBeenCalledTimes(0);
    expect(alerts.addRecoverAlert).toHaveBeenCalledTimes(1);
});

test('Previous "Recovered" alert, load avg > 1', () => {
    const loadAverage = 1.3;
    const row = { warning: 0 };

    alerts.triggerAlert(datetime, datetimeSeconds, loadAverage, row);
    
    // Show add a warning alert as the last alert was a recovery
    expect(alerts.addWarningAlert).toHaveBeenCalledTimes(1);
    expect(alerts.addRecoverAlert).toHaveBeenCalledTimes(0);
});

test('Previous "Recovered" alert, load avg <= 1', () => {
    const loadAverage = 0.8;
    const row = { warning: 0 };

    alerts.triggerAlert(datetime, datetimeSeconds, loadAverage, row);
    
    // Show not add an alert as the last alert was a recovery
    expect(alerts.addWarningAlert).toHaveBeenCalledTimes(0);
    expect(alerts.addRecoverAlert).toHaveBeenCalledTimes(0);
});